<?php

namespace TimeTracking;

return [
	'doctrine' => [
		'driver' => [
			// defines an annotation driver with two paths, and names it `my_annotation_driver`
			__NAMESPACE__.'_driver' => [
				'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
				'cache' => 'array',
				'paths' => [
					__DIR__ . '/../src/'.__NAMESPACE__.'/Model/Entity',
				],
			],
			'orm_default' => [
				'drivers' => [
					__NAMESPACE__.'\Model\Entity' => __NAMESPACE__.'_driver',
				],
			],
		],
	],
	'view_manager' => [
		'template_path_stack' => [
			__DIR__ . '/../view',
		],
	],
	'controllers' => [
		'abstract_factories' => [
			'TimeTracking\Controller\Project' => 'TimeTracking\Factory\AbstractControllerFactory',
			'TimeTracking\Controller\LogTime' => 'TimeTracking\Factory\AbstractControllerFactory'
		]
	],
	// This lines opens the configuration for the RouteManager
	'router' => [
		// Open configuration for all possible routes
		'routes' => [
			// Define a new route called "post"
			'projects' => [
				// Define the routes type to be "Zend\Mvc\Router\Http\Literal", which is basically just a string
				'type' => 'segment',
				// Configure the route itself
				'options' => [
					// Listen to "/blog" as uri
					'route'    => '/projects[/][:action][/:id]',
					'constraints' => [
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'     => '[0-9]+',
					],
					// Define default controller and action to be called when this route is matched
					'defaults' => [
						'controller' => 'TimeTracking\Controller\Project',
						'action' => 'list'
					]
				]
			],
			'logTime' => [
				// Define the routes type to be "Zend\Mvc\Router\Http\Literal", which is basically just a string
				'type' => 'segment',
				// Configure the route itself
				'options' => [
					// Listen to "/blog" as uri
					'route'    => '/logs[/:id][/][:action]',
					'constraints' => [
						'id'     => '[0-9]+',
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					],
					// Define default controller and action to be called when this route is matched
					'defaults' => [
						'controller' => 'TimeTracking\Controller\LogTime',
						'action' => 'loggedTimes'
					]
				]
			]
		]
	]
];
