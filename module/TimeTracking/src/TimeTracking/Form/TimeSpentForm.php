<?php

namespace TimeTracking\Form;

use Zend\Form\Form;

class TimeSpentForm extends Form
{
	public function __construct($name = null)
	{
		parent::__construct('timeSpent');
		$this->add([
			'name' => 'id',
			'type' => 'Hidden',
			'attributes' => [
				'value' => uniqid(),
			],
		]);
		$this->add([
			'name' => 'startDate',
			'type' => 'Zend\Form\Element\Date',
			'options' => [
				'label' => 'Start date',
				'format' => 'Y-m-d'
			],
		]);
		$this->add([
			'name' => 'startTime',
			'type' => 'Zend\Form\Element\Time',
			'options' => [
				'label' => 'Start Time',
				'format' => '00:00:00',
			],
			'attributes' => [
				'value' => date('H:i:s'),
				'min' => '00:00:00',
				'max' => '23:59:59',
				'step' => '1',
			]
		]);
		$this->add([
			'name' => 'stopTime',
			'type' => 'Zend\Form\Element\Time',
			'options' => [
				'label' => 'Stop Time',
				'format' => '00:00:00',
				'required' => false,
			],
		]);
		$this->add([
			'name' => 'stopDate',
			'type' => 'Zend\Form\Element\Date',
			'options' => [
				'label' => 'Stop date',
				'format' => 'Y-m-d',
			],
			'attributes' => [
				'required' => false,
			],
		]);
		$this->add([
			'name' => 'submit',
			'type' => 'Submit',
			'attributes' => [
				'value' => 'Start',
				'id' => 'submitbutton',
			],
		]);
	}
}
