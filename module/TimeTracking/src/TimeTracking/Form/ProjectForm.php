<?php

namespace TimeTracking\Form;

use Zend\Form\Form;

class ProjectForm extends Form
{
	public function __construct($name = null)
	{
		parent::__construct('project');
		$this->add([
			'name' => 'id',
			'type' => 'Hidden',
		]);
		$this->add([
			'name' => 'name',
			'type' => 'Text',
			'options' => [
				'label' => 'Name',
			],
			'attributes' => [
				'class' => 'form-control',
			],
		]);
		$this->add([
			'name' => 'startDate',
			'type' => 'Zend\Form\Element\Date',
			'options' => [
				'label' => 'Start date',
				'format' => 'Y-m-d'
			],
			'attributes' => [
				'class' => 'form-control',
			],
		]);
		$this->add([
			'name' => 'endDate',
			'type' => 'Zend\Form\Element\Date',
			'required' => false,
			'options' => [
				'label' => 'Finish date',
				'format' => 'Y-m-d'
			],
			'attributes' => [
				'class' => 'form-control',
				'required' => false,
			],
		]);
		$this->add([
			'name' => 'submit',
			'type' => 'Submit',
			'attributes' => [
				'value' => 'Create',
				'id' => 'submitbutton',
				'class' => 'btn btn-primary'
			],
		]);
	}
}
