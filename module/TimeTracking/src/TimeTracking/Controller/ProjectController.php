<?php

namespace TimeTracking\Controller;

use TimeTracking\Exception\ProjectNotFoundException;
use TimeTracking\Form\ProjectForm;
use TimeTracking\Model\Repository\ProjectRepositoryInterface;
use Zend\Mvc\Controller\AbstractActionController;
use TimeTracking\Model\Entity\Project;

class ProjectController extends AbstractActionController {

	/**
	 * @var ProjectRepositoryInterface
	 */
	protected $projectRepo;

	public function __construct(ProjectRepositoryInterface $projectRepo)
	{
		$this->projectRepo = $projectRepo;
	}

	public function listAction()
	{
		return [
			'projects' => $this->projectRepo->getAll(),
		];
	}

	public function editAction()
	{
		$id = $this->getIdParameter();

		try{
			$project = $this->projectRepo->get($id);
		} catch(ProjectNotFoundException $e){
			return $this->redirect()->toRoute('projects', array(
				'action' => 'add'
			));
		}

		$form  = new ProjectForm();
		$form->bind($project);
		$form->get('submit')->setAttribute('value', 'Edit');
		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->setInputFilter($project->getInputFilter());
			$form->setData($request->getPost());
			if ($form->isValid()) {
				$this->projectRepo->save($project);
				// Redirect to list of projects
				return $this->redirect()->toRoute('projects');
			}
		}
		return [
			'id' => $id,
			'form' => $form,
		];
	}

	public function addAction()
	{
		$form = new ProjectForm();
		$form->get('submit')->setValue('Add');

		$request = $this->getRequest();
		$data = ['form' => $form];
		if ($request->isPost())
		{
			$project = new Project();
			$form->setInputFilter($project->getInputFilter());
			$form->setData($request->getPost());

			if (!$form->isValid())
			{
				return $data;
			}
			$project->exchangeArray($form->getData());
			if(!$this->projectRepo->exists($project->getName()))
			{
				$this->projectRepo->save($project);
				// Redirect to project list
				return $this->redirect()->toRoute('projects');
			}
			$data['message'] = "A project with this name exists already. Project Name has to be unique";
		}
		return $data;
	}

	public function deleteAction()
	{
		$id = $this->getIdParameter();
		$request = $this->getRequest();
		if (!$request->isPost())
		{
			return ['id' => $id, 'project' => $this->projectRepo->get($id)];
		}

		$del = $request->getPost('del', 'No');
		if ($del == 'Yes') {
			$id = (int) $request->getPost('id');
			try
			{
				$project = $this->projectRepo->get($id);
				$this->projectRepo->delete($project);
			} catch(ProjectNotFoundException $e)
			{
			}
		}
		return $this->redirect()->toRoute('projects');
	}

	/**
	 * Retrieve the id parameter from the request. if not found redirect
	 * to projects add route.
	 *
	 * @return int
	 */
	private function getIdParameter()
	{
		$id = (int)$this->params()
		                ->fromRoute('id', 0);
		if (!$id)
		{
			return $this->redirect()
			            ->toRoute('projects', array('action' => 'add'));
		}

		return $id;
	}
}
