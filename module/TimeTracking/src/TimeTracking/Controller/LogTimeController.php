<?php

namespace TimeTracking\Controller;

use TimeTracking\Exception\ProjectNotFoundException;
use TimeTracking\Form\TimeSpentForm;
use TimeTracking\Model\Entity\TimeSpent;
use TimeTracking\Model\Repository\ProjectRepositoryInterface;
use TimeTracking\Model\Repository\TimeSpentRepositoryInterface;
use Zend\Mvc\Controller\AbstractActionController;

class LogTimeController extends AbstractActionController
{
	/**
	 * @var ProjectRepositoryInterface
	 */
	protected $projectRepo;

	public function __construct(ProjectRepositoryInterface $projectRepo, TimeSpentRepositoryInterface $timeRepo)
	{
		$this->projectRepo = $projectRepo;
		$this->timeSpentRepo = $timeRepo;
	}

	public function loggedTimesAction()
	{
		$id = $this->getIdParameter();

		try
		{
			$projet = $this->projectRepo->get($id);
		} catch (ProjectNotFoundException $e){
			return $this->redirect()->toRoute('projects');
		}

		return [
			'project' => $projet,
			'loggedTimes' => $this->projectRepo->getLoggedTimes($id)
		];
	}

	public function startLoggingAction()
	{
		$id = $this->getIdParameter();
		$message = '';
		try{
			$project = $this->projectRepo->get($id);
			if($this->projectRepo->hasRunningLog($id)){
				return $this->redirect()->toRoute('logTime', array(
					'action' => 'stopRunningLog',
					'id' => $id
				));
			};
		} catch(ProjectNotFoundException $e){
			return $this->redirect()->toRoute('project', array(
				'action' => 'add'
			));
		}

		$request = $this->getRequest();
		if ($request->isPost()) {
			$startTime = $request->getPost()->get('startTime');
			$timeRegex = '/^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$/';
			if(preg_match($timeRegex, $startTime, $matches))
			{
				$timeSpent = new TimeSpent();
				list($full,) = $matches;
				$timeSpent->setStartTime($full);
				$now = date('Y-m-d');
				$timeSpent->setStartDate($now);
				$timeSpent->setStopDate($now);
				$timeSpent->setProject($project);
				$project->logTime($timeSpent);
				$this->projectRepo->save($project);

				return $this->redirect()->toRoute('projects');
			}
			$message = "The input entered is not a time. Accepted Format hh:mm:ss";
		}
		return [
			'message' => $message,
			'project' => $project,
		];
	}

	public function runningLogAction(){
		return ['runningLogs' => $this->timeSpentRepo->getRunningLog()];
	}

	public function stopRunningLogAction(){
		$id = $this->getIdParameter();
		$request = $this->getRequest();
		if ($request->isPost()) {
			$stopTime = $request->getPost()->get('stopTime');
			$timeRegex = '/^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$/';

			if(preg_match($timeRegex, $stopTime, $matches))
			{
				$timeSpent = $this->projectRepo->getRunningLog($id);
				list($full,) = $matches;
				$timeSpent->setStopTime($full);
				$timeSpent->setStopDate(date('Y-m-d'));
				$this->timeSpentRepo->save($timeSpent);

				return $this->redirect()->toRoute('logTime');
			}
			$message = "The input entered is not a time. Accepted Format hh:mm:ss";
		}

		$data = ['runningLog' => $this->projectRepo->getRunningLog($id)];
		if(isset($message))
		{
			$data['message'] = $message;
		}
		return $data;
	}

	/**
	 * Retrieve the id parameter from the request. if not found redirect
	 * to projects add route.
	 *
	 * @return int
	 */
	private function getIdParameter()
	{
		$id = (int)$this->params()
		                ->fromRoute('id', 0);
		if (!$id)
		{
			return $this->redirect()
			            ->toRoute('projects', ['action' => 'add']);
		}

		return $id;
	}
} 