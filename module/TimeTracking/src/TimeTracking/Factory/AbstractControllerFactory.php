<?php

namespace TimeTracking\Factory;

use TimeTracking\Model\Repository\ProjectRepository;
use TimeTracking\Model\Repository\TimeSpentRepository;
use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AbstractControllerFactory implements AbstractFactoryInterface
{
	/**
	 * Determine if we can create a service with name
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	 * @param $name
	 * @param $requestedName
	 * @return bool
	 */
	public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
	{
		return class_exists($requestedName.'Controller');
	}

	/**
	 * Create service with name
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	 * @param $name
	 * @param $requestedName
	 * @return mixed
	 */
	public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
	{
		$realServiceLocator = $serviceLocator->getServiceLocator();
		$em = $realServiceLocator->get('Doctrine\ORM\EntityManager');
		$meta = $em->getClassMetadata('TimeTracking\Model\Entity\Project');
		$projectRepository = new ProjectRepository($em, $meta);
		$controller = $requestedName.'Controller';

		if($controller === 'TimeTracking\Controller\LogTimeController')
		{
			$meta = $em->getClassMetadata('TimeTracking\Model\Entity\TimeSpent');
			$timeRepository = new TimeSpentRepository($em, $meta);

			return new $controller($projectRepository, $timeRepository);
		}

		return new $controller($projectRepository);
	}
}