<?php

namespace TimeTracking\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * TimeSpent
 *
 * @ORM\Table(name="time_spent", indexes={@ORM\Index(name="IDX_B417D625166D1F9C", columns={"project_id"})})
 * @ORM\Entity
 */
class TimeSpent implements InputFilterAwareInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="time_spent_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="start_date", type="string", nullable=true)
     */
    private $startDate;

    /**
     * @var string
     *
     * @ORM\Column(name="start_time", type="string", nullable=true)
     */
    private $startTime;

    /**
     * @var string
     *
     * @ORM\Column(name="stop_time", type="string", nullable=true)
     */
    private $stopTime;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="stop_date", type="string", nullable=true)
	 */
	private $stopDate;

	protected $inputFilter;

    /**
     * @var \TimeTracking\Model\Entity\Project
     *
     * @ORM\ManyToOne(targetEntity="TimeTracking\Model\Entity\Project", inversedBy="loggedTimes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

	/**
	 * @return string
	 */
	public function getStopDate()
	{
		return $this->stopDate;
	}

	/**
	 * @param string $stopDate
	 */
	public function setStopDate($stopDate)
	{
		$this->stopDate = $stopDate;
	}


	/**
	 * @return string
	 */
	public function getStopTime()
	{
		return $this->stopTime;
	}

	/**
	 * @param string $endTime
	 * @return $this
	 */
	public function setStopTime($endTime)
	{
		$this->stopTime = $endTime;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return Project
	 */
	public function getProject()
	{
		return $this->project;
	}

	/**
	 * @param Project $project
	 * @return $this
	 */
	public function setProject($project)
	{
		$this->project = $project;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getStartTime()
	{
		return $this->startTime;
	}

	/**
	 * @param string $startTime
	 * @return $this
	 */
	public function setStartTime($startTime)
	{
		$this->startTime = $startTime;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getStartDate()
	{
		return $this->startDate;
	}

	/**
	 * @param string $trackingDate
	 * @return $this
	 */
	public function setStartDate($trackingDate)
	{
		$this->startDate = $trackingDate;

		return $this;
	}

	/**
	 * Set input filter
	 *
	 * @param  InputFilterInterface $inputFilter
	 * @throws \Exception
	 * @return InputFilterAwareInterface
	 */
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}

	/**
	 * Retrieve input filter
	 *
	 * @return InputFilterInterface
	 */
	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();
			$inputFilter->add([
				'name'     => 'id',
				'required' => true,
			]);
			$inputFilter->add([
				'name' => 'startDate',
				'required' => false,
				'options' => [
					'label' => 'Start Date Date/Time',
					'format' => 'Y-m-d'
				],
				'attributes' => [
					'min' => date('Y-m-d'),
					'max' => '2020-06-02',
					'step' => '1', // minutes; default step interval is 1 min
				]
			]);
			$inputFilter->add([
				'name' => 'endDate',
				'required' => false,
				'options' => [
					'label' => 'End Date Date/Time',
					'format' => 'Y-m-d'
				],
				'attributes' => [
					'min' => date("Y-m-d", time() + 86400),
					'max' => '2020-06-02',
					'step' => '1', // minutes; default step interval is 1 min
				]
			]);
			$inputFilter->add([
				'name' => 'startTime',
				'options'=> [
					'label'  => 'Start Time',
				],
			]);
			$inputFilter->add([
				'name' => 'stopTime',
				'required' => false,
				'options'=> [
					'label'  => 'Start Time',
					'format' => 'H:i:s'
				],
				'attributes' => [
					'min' => '00:00:00',
					'max' => '23:59:00',
					'step' => '1', // seconds; default step interval is 60 seconds
				]
			]);
			$this->inputFilter = $inputFilter;
		}
		return $this->inputFilter;
	}

	/**
	 * Convert the object to an array.
	 *
	 * @return array
	 */
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}

	/**
	 * Populate from an array.
	 *
	 * @param array $data
	 */
	public function exchangeArray($data = array())
	{
		$this->id = $data['id'];
		$this->startDate = $data['startDate'];
		$this->startTime = $data['startTime'];
		$this->stopDate = $data['stopDate'];
		$this->stopTime = $data['stopTime'];
	}
}
