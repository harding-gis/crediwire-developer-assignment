<?php

namespace TimeTracking\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * Project
 *
 * @ORM\Table(name="project", uniqueConstraints={@ORM\UniqueConstraint(name="project_name_key", columns={"name"})})
 * @ORM\Entity
 */
class Project implements InputFilterAwareInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="project_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="string", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="string", options={"default" = null})
     */
    private $endDate;

	/**
	 * @ORM\OneToMany(targetEntity="TimeTracking\Model\Entity\TimeSpent", mappedBy="project", cascade={"persist", "remove"})
	 */
	private $loggedTimes;

	protected $inputFilter;

	/**
	 * constructor
	 */
	public function __construct()
	{
		$this->loggedTimes = new ArrayCollection();
	}

	/**
	 * @param TimeSpent $time
	 * @return $this
	 */
	public function logTime(TimeSpent $time)
	{
		$this->loggedTimes->add($time);

		return $this;
	}

	/**
	 * @param TimeSpent $timeSpent
	 * @return $this
	 */
	public function removeLoggedTime(TimeSpent $timeSpent)
	{
		$this->loggedTimes->removeElement($timeSpent);

		return $this;
	}

	/**
	 * @return ArrayCollection
	 */
	public  function getLoggedTimes()
	{
		return $this->loggedTimes;
	}

	/**
	 * @return \DateTime
	 */
	public function getEndDate()
	{
		return $this->endDate ;
	}

	/**
	 * @param \DateTime $endDate
	 * @return $this
	 */
	public function setEndDate($endDate)
	{
		if($endDate == '') $endDate = null;
		$this->endDate = $endDate;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return $this
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getStartDate()
	{
		return $this->startDate;
	}

	/**
	 * @param \DateTime $startDate
	 * @return $this
	 */
	public function setStartDate($startDate)
	{
		$this->startDate = $startDate;

		return $this;
	}

	/**
	 * Convert the object to an array.
	 *
	 * @return array
	 */
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}

	/**
	 * Populate from an array.
	 *
	 * @param array $data
	 */
	public function exchangeArray($data = array())
	{
		$this->id = $data['id'];
		$this->name = $data['name'];
		$this->startDate = $data['startDate'];
		$this->endDate = $data['endDate'];
	}

	/**
	 * Set input filter
	 *
	 * @param  InputFilterInterface $inputFilter
	 * @throws \Exception
	 * @return InputFilterAwareInterface
	 */
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}

	/**
	 * Retrieve input filter
	 *
	 * @return InputFilterInterface
	 */
	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();
			$inputFilter->add(array(
				'name'     => 'id',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			));
			$inputFilter->add(array(
				'name'     => 'name',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 50,
						),
					),
				),
			));
			$inputFilter->add(array(
				'name' => 'startDate',
				'options' => array(
					'label' => 'Start Date Date/Time',
					'format' => 'Y-m-d'
				),
				'attributes' => array(
					'min' => '2017-06-12',
					'max' => '2020-06-02',
					'step' => '1', // minutes; default step interval is 1 min
				)
			));
			$inputFilter->add(array(
				'name' => 'endDate',
				'required' => false,
				'options' => array(
					'label' => 'End Date Date/Time',
					'format' => 'Y-m-d'
				),
				'attributes' => array(
					'min' => '2017-06-12',
					'max' => '2020-06-02',
					'step' => '1', // minutes; default step interval is 1 min
				)
			));
			$this->inputFilter = $inputFilter;
		}
		return $this->inputFilter;
	}
}
