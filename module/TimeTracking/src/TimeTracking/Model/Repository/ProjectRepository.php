<?php

namespace TimeTracking\Model\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\NonUniqueResultException;
use TimeTracking\Exception\RunningLogNotFoundException;
use TimeTracking\Model\Entity\Project;

class ProjectRepository extends EntityRepository implements ProjectRepositoryInterface
{

	/**
	 * @param EntityManager $entityManager
	 * @param ClassMetadata $classMetadata
	 */
	public function __construct(EntityManager $entityManager, ClassMetadata $classMetadata)
	{
		parent::__construct($entityManager, $classMetadata);
	}

	/**
	 * Get all the project
	 *
	 * @return array Project
	 */
	public function getAll()
	{
		return $this->findAll();
	}

	/**
	 * @param $id
	 * @return Project
	 * @throws ProjectNotFoundException
	 */
	public function get($id){
		$found = $this->find($id);

		if(!$found){
			throw new ProjectNotFoundException($id);
		}

		return $found;
	}


	/**
	 * @param Project $project
	 * @return mixed
	 */
	public function save(Project $project)
	{
		if($project->getEndDate() == ""){
			$project->setEndDate(null);
		}
		$this->_em->persist($project);
		$this->_em->flush();
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function getLoggedTimes($id)
	{
		$project = $this->get($id);
		return $project->getLoggedTimes();
	}

	public function exists($name)
	{
		return ($this->findOneBy(['name' => $name]));
	}

	/**
	 * @param Project $project
	 * @return mixed
	 */
	public function delete(Project $project)
	{
		$this->_em->remove($project);
		$this->_em->flush();
	}

	/**
	 * @param $id
	 * @return bool
	 */
	public function hasRunningLog($id)
	{
		$query = $this->_em->createQuery("SELECT t FROM '\\TimeTracking\\Model\\Entity\\TimeSpent' t JOIN t.project p WHERE (p.id = {$id} AND t.stopTime IS NULL) ");
		$result = $query->getResult();
		return !empty($result);
	}

	/**
	 * @param $id
	 * @throws RunningLogNotFoundException
	 * @return mixed
	 */
	public function getRunningLog($id)
	{
		if($this->hasRunningLog($id))
		{
			$r = $this->_em
				->createQuery("SELECT t FROM '\\TimeTracking\\Model\\Entity\\TimeSpent' t JOIN t.project p WHERE (p.id = {$id} AND t.stopTime IS NULL) ")
				->getResult()[0];
			return $r;
		}
	}
}
 