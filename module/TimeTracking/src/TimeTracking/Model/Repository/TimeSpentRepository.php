<?php

namespace TimeTracking\Model\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use TimeTracking\Model\Entity\TimeSpent;

class TimeSpentRepository extends EntityRepository implements TimeSpentRepositoryInterface {

	/**
	 * @param EntityManager $entityManager
	 * @param ClassMetadata $classMetadata
	 */
	public function __construct(EntityManager $entityManager, ClassMetadata $classMetadata)
	{
		parent::__construct($entityManager, $classMetadata);
	}

	public function getRunningLog()
	{
		return $this->findBy(['stopTime' => null]);
	}

	public function hasRunningLog($id)
	{

	}

	/**
	 * @param TimeSpent $timeSpent
	 * @return mixed
	 */
	public function save(TimeSpent $timeSpent)
	{
		$this->_em->persist($timeSpent);
		$this->_em->flush();
	}
}