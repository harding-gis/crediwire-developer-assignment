<?php

namespace TimeTracking\Model\Repository;

use TimeTracking\Model\Entity\TimeSpent;

interface TimeSpentRepositoryInterface {

	public function getRunningLog();

	/**
	 * @param int $id
	 * @return mixed
	 */
	public function hasRunningLog($id);

	/**
	 * @param TimeSpent $timeSpent
	 * @return mixed
	 */
	public function save(TimeSpent $timeSpent);
} 