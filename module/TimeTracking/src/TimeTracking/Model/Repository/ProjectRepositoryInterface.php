<?php

namespace TimeTracking\Model\Repository;

use TimeTracking\Model\Entity\Project;

interface ProjectRepositoryInterface {

	/**
	 * Get all the project
	 *
	 * @return array Project
	 */
	public function getAll();

	/**
	 * @param Project $project
	 * @return mixed
	 */
	public function save(Project $project);

	/**
	 * @param $id
	 * @return Project
	 */
	public function get($id);

	/**
	 * @param $id
	 * @return mixed
	 */
	public function getLoggedTimes($id);

	/**
	 * @param $name
	 * @return mixed
	 */
	public function exists($name);

	/**
	 * @param Project $project
	 * @return mixed
	 */
	public function delete(Project $project);

	/**
	 * @param $id
	 * @return mixed
	 */
	public function hasRunningLog($id);

	/**
	 * @param $id
	 * @return mixed
	 */
	public function getRunningLog($id);
}
