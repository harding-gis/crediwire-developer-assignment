<?php

namespace TimeTracking\Exception;

use Exception;

class ProjectNotFoundException extends Exception {

	public function __construct($id, $code = 0, Exception $previous = null)
	{
		$message = sprintf("Cannot Found project with the id: %d", $id);
		parent::__construct($message, $code, $previous);
	}

	public function __toString() {
		return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
	}
} 